
(function($, window) {

	var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

//Window load
$(window).load(function() {
	if($("body").hasClass("homep")) {
		$(".container").animate({opacity:1}, 500);
	}
});

//Arrange divs based on screen size
function _fixDivs() {
    var width = parseInt($(window).width());
    if(width <= 900) {
        $('.pan-right').insertBefore('.pan-left');
        $(".navigation").height(screen.height);
    }
    else { $('.pan-left').insertBefore('.pan-right'); $(".navigation").show().height(50) }
};


 function onOrientationChange()
  {
    switch(window.orientation) 
    {  
      case -90:
      case 90:
        
        break; 
      default:
      //portrait
        $(".navigation").hide();
        $(".collapse").css("display", "none");
        break; 
    }
  }


window.addEventListener('orientationchange', onOrientationChange);
$(window).resize(_fixDivs);

/*mobile, ipad, touch...*/
if(isMobile.any()) {
    _toggleMenu = function(e) {
        e.stopPropagation(); e.preventDefault();
        if($(".navigation").is(':visible')) {
            $(this).removeClass("sub-toggle");
            $(".navigation").hide();
            $(".collapse").css("display", "none");
        }
        else {
            $(this).addClass("sub-toggle"); 
            $(".navigation").show(); 
            $(".collapse").css("display", "block");
        }
        return true;
    };
    $(".collapse").css("display", "none").height(screen.height);
    $(".navigation").hide().height(screen.height);
    $(".navbar-toggle").on("click touchstart", _toggleMenu );   
}

_fixDivs();

})(jQuery, window);
