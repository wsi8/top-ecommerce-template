<?php include_once("partials/head.php"); ?>
<body class="no-js no-js detailp">

	<?php include_once("partials/top.php"); ?>
	<?php include_once("partials/nav.php"); ?>

	<!-- titles -->
	<div class="container">
		<div class="content-title-simple">Sei già iscritto? <strong>Clicca qui</strong> ed effettua il login per procedere all'acquisto</div>
		<div class="row r1">
			<div class="col-md-12"></div>
		</div>


		<!-- cont cart -->
		<div class="row cart no-margin">

			<!-- col left -->
			<div class="col-md-6 cpanel">
				<h1>01 <span class="color-pink">INDIRIZZO DI SPEDIZIONE</span></h1>
				<div class="row">
					<div class="col-md-6">
						<div class="inputs-title">
							<div class="colored-box cradio _radio-sel bg-white" name="customer"></div>
							<div class="text">Privato</div>
						</div>
						<div class="inputs-list">
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Nome"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Cognome"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Email"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Conferma email"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Password"/></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputs-title">
							<div class="colored-box bg-white cradio" name="customer"></div>
							<div class="text">Azienda</div>
						</div>
						<div class="inputs-list">
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Conferma password"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Indirizzo"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="CAP"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Provincia"/></div>
							<div class="input-cont"><input type="text" class="grey-input" placeholder="Città"/></div>
						</div>
					</div>
				</div>
			</div>

			<!-- col right -->
			<div class="col-md-6 cpanel">
				<h1>02 <span class="color-pink">OPZIONI DI SPEDIZIONE</span></h1>
				<h2>SPEDIZIONI VALIDE PER L'ITALIA</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="inputs-title">
							<div class="colored-box _radio-sel bg-white cradio" name="ship-type"></div>
							<div class="text f-left">Standard</div>
							<div class="text f-right">&euro;5,00</div>
						</div>
						<h2 class="fsize-15">3-5 giorni lavorativi</h2>
					</div>
					<div class="col-md-6">
						<div class="inputs-title">
							<div class="colored-box bg-white cradio" name="ship-type"></div>
							<div class="text f-left">Express</div>
							<div class="text f-right">&euro;12,00</div>
						</div>
						<h2 class="fsize-15">1-2 giorni lavorativi</h2>
					</div>
				</div>
				<br/><br/>
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio"></div>
					<div class="text f-left cradio">Spedizioni Europa</div>
					<div class="text f-right">&euro;20,00</div>
				</div>
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio"></div>
					<div class="text f-left">Spedizioni Usa</div>
					<div class="text f-right">&euro;43,00</div>
				</div>
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio"></div>
					<div class="text f-left">Spedizioni Asia</div>
					<div class="text f-right">&euro;62,00</div>
				</div>
			</div>
		</div>


		<!-- payments methods -->
		<div class="row cart payments-methods">
			<div class="col-md-4 cpanel">
				<h1 class="no-border" style="margin-top:10px;">03 <span class="color-pink">OPZIONI DI SPEDIZIONE</span></h1>
			</div>
			<div class="col-md-3">
			<div class="inputs-title bg-white">
				<div class="colored-box bg-white cradio _radio-sel" name="payment-type"></div>
				<div class="text">Carta di credito</div>
			</div>
			</div>
			<div class="col-md-3">
				<div class="inputs-title bg-white">
					<div class="colored-box bg-white cradio" name="payment-type"></div>
					<div class="text">Contanti alla consegna</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="inputs-title bg-white">
					<div class="colored-box bg-white cradio" name="payment-type"></div>
					<div class="text">Paypal</div>
				</div>
			</div>
		</div>


		<div class="row cart">
			<div class="col-md-12 cpanel">
				<h1 class="no-border" style="margin-top:10px;">04 <span class="color-pink">IL TUO ORDINE</span></h1>
			</div>
		</div>


		<div class="row cart">
			<div class="col-md-6 cpanel">
				<table class="table ship-summary">
					<thead>
						<tr>
							<th class="product" >PRODOTTO</th><th class="qty">QT</th><th class="price">SUBTOTALE</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="product"><div class="colored-box bg-black"></div>Zebra glicine</td>
							<td class="qty metersRequested">2.75</td>
							<td class="price totalRequested">&euro; 60.00</td>
						</tr>
						<tr>
							<td class="product"></td>
							<td class="qty">SPEDIZIONE</td>
							<td class="price">&euro; 5.00</td>
						</tr>
						<tr>
							<td class="product"></td>
							<td class="qty">PAGAMENTO</td>
							<td class="price">&euro; 0.00</td>
						</tr>
					</tbody>
				</table>
				<div class="total-box">
					<div class="f-left"><span class="color-pink">TOTALE </span>ORDINE</div>
					<div class="f-right totalComputed">&euro;65.00</div>
				</div>
			</div>
			<div class="col-md-6 cpanel">
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio"></div>
					<div class="text f-left">Iscriviti alla newsletter</div>
				</div>
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio _radio-sel"></div>
					<div class="text f-left">Accetto le condizioni di vendita*</div>
				</div>
				<div class="inputs-title mbottom-20">
					<div class="colored-box bg-white cradio _radio-sel"></div>
					<div class="text f-left">Accetto l'informativa sulla privacy*</div>
				</div>
				<div class="send-order-cont">
					<a href="index.php?m=ord-sent" class="send-order bg-pink">INVIA ORDINE</a>
				</div>
			</div>
		</div>


	</div>
	<script>

		//Check storage
		if(localStorage.getItem("products")) {
			$(".cart-total").html(localStorage.getItem("products")).show();
		}
		if(localStorage.getItem("totalRequested")) {
			var treq = localStorage.getItem("totalRequested");
			$(".totalRequested").html("&euro;" + treq);
			$(".totalComputed").html("&euro;" + (parseFloat(treq) + 5).toFixed(2));
		}
		if(localStorage.getItem("metersRequested")) {
			$(".metersRequested").html(localStorage.getItem("metersRequested"));
		}


		/*CRADIO event*/
		$("div.cradio").on("click touchstart", function() {
			var n = $(this).attr("name");
			if($("div.cradio[name='"+n+"']").hasClass("_radio-sel")) {
				$("div.cradio[name='"+n+"']").removeClass("_radio-sel");
			} 
			if($(this).hasClass("_radio-sel"))
				$(this).removeClass("_radio-sel");
			else $(this).addClass("_radio-sel")
		});
	</script>

	<?php include_once("partials/footer.php");?>