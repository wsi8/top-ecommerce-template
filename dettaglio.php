<?php include_once("partials/head.php"); ?>
<body class="no-js homep">

	<?php include_once("partials/top.php"); ?>
	<?php include_once("partials/nav.php"); ?>

	<div class="container m-body">
			<div class="content-title">TUTTI I TESSUTI PER LA CASA</div>
		<div class="row r1">
			<div class="col-md-12">
				<div class="row detail-title">
					<div class="col-md-9">TESSUTO <strong>ZEBRA</strong></div>
					<div class="col-md-3">costo al metro lineare &euro;22.00</div>
				</div>
			</div>
		</div>

		<!-- cont detail -->
		<div class="row product-detail">
			<div class="col-md-4">
				<div class="fabric-preview">
					<div class="color-cover"></div>
					<img src="./resources/images/zebra.jpg" />
				</div>
			</div>
			<div class="col-md-1 color-preview">
				<div class="csample hidden-xs"></div>
			</div>
			<div class="col-md-7">
				<div class="row">
					<!-- colors -->
					<div class="col-md-4" style="text-align:center;">
						<p>SELEZIONA IL COLORE</p>
						<div class="color color-1"></div>
						<div class="color color-2"></div>
						<div class="color color-3"></div>
						<div class="color color-4"></div>
					</div>
					<script>
					$(".color").on("click", function() {
						$(".csample").css("backgroundColor", $(this).css("backgroundColor"));
						$(".color-cover").css("backgroundColor", $(this).css("backgroundColor"));
					}).on("mouseover", function() { 
						$(this).css("cursor", "pointer"); 
						$(".csample").css("backgroundColor", $(this).css("backgroundColor"));
						var topval;
						topval = $(this).position().top;
						$(".csample").stop(true, false).animate({top: topval}, 300);
					});
					</script>
					<!-- info -->
					<div class="col-md-8">
						<section class="info-table">
							<div class="info-part">
								<p>descrizione del tessuto: <strong>pois</strong></p>
							</div>
							<div class="info-part">
								<p>compisizione: <strong>cotone egiziano</strong></p>
							</div>
							<div class="info-part f-left">
								<p>colore: <strong>glicine</strong></p>
							</div>
							<div class="info-part f-left">
								<p>titolo filato: <strong>80/2, 80/2</strong></p>
							</div>
							<div class="info-part f-left">
								<p>altezza: <strong>148/150cm</strong></p>
							</div>
							<div class="info-part f-left">
								<p>disponibilità: <strong>immediata</strong></p>
							</div>
							<div class="info-part">
								<p>COSTO AL METRO LINEARE: &euro; 22.00</p>
							</div>
							<div class="info-part">
								<label class="message">indica quanti metri lineari ti occorrono</label>
								<input type="text" name="meters" value="1" />
							</div>
							<div class="info-part">
								<div class="total-fill">
									<span class="f-left">TOTALE:</span>
									<span class="f-right total-euro">&euro;22.00</span>
								</div>
								<script>
								$("input[name='meters']").on("keyup", function() {
									var tot = parseFloat($(this).val() * 22.00).toFixed(2); 
								 	$(".total-euro").html("&euro;" + tot);
								 	localStorage.setItem("totalRequested", tot);
								 	localStorage.setItem("metersRequested", $(this).val());
								});
								</script>
							</div>
						</section>
					</div>
				</div>
			</div>

			<!-- shipments -->
			<div class="row">
				<div class="col-md-12 shipments-info">
					<div class="row no-margin">
						<div class="col-md-3 pan"><p class="large-text">SPEDIZIONI</p></div>
						<div class="col-md-3 pan">
							<p>STANDARD</p>
							<p>EXPRESS*</p>
						</div>
						<div class="col-md-3 pan">
							<p>CONSEGNA ENTRO 3/5 GIORNI LAVORATIVI</p>
							<p>CONSEGNA ENTRO 1/2 GIORNI LAVORATIVI</p>
						</div>
						<div class="col-md-3 pan no-border">
							<p>&euro; 5,00</p>
							<p>&euro; 12,00 </p>
						</div>
					</div>
				</div>
			</div>


			<!-- buttons -->
			<div class="row buttons-tools">
				<div class="col-md-6">
					<div class="tbtn btn-1 bg-pink">
						<a href="#" action="tocart">AGGIUNGI AL CARRELLO</a>
					</div>
					<script>
					$("a[action='tocart']").on("click", function(e) {
						e.preventDefault(); e.stopPropagation();
						var clone = $(".fabric-preview").clone();
						$(clone).css("position", "absolute").css("top", $(".fabric-preview").offset().top).css("left", $(".fabric-preview").offset().left).css("opacity", 0.7).css("overflow", "hidden");
						$("body").append(clone);
						$(clone).stop(true, false).animate({ 
							top: $(".icon-cart").offset().top, 
							left: $(".icon-cart").offset().left, 
							opacity:0,
							width:$(".icon-cart").width(),
							height:$(".icon-cart").height()  
						}, {
							duration:500,
							complete: function() {
								$(clone).remove();
								var tot = parseInt(localStorage.getItem("products"));
								if(!tot) tot = 0;
								localStorage.setItem("products", tot + 1);
								$(".cart-total").html(localStorage.getItem("products")).show();
								$(".icon-cart").animate({ marginTop: -20}, {
									duration: 200,
									complete: function() {
										$(".icon-cart").animate({ marginTop: 0}, 200);
									}
								});
							}
						});
					});
					</script>
					<div class="tbtn btn-2">
						<a href="carrello.php">ACQUISTA SUBITO</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="tbtn btn-3">
						<a href="index.php">NELLA LISTA DESIDERI</a>
					</div>
					<div class="tbtn btn-4">
						<a href="index.php">TORNA ALLE PROMOZIONI</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<?php include_once("partials/footer.php");?>