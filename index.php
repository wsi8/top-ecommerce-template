<?php include_once("partials/head.php"); ?>
<body class="no-js homep">

	<?php include_once("partials/top.php"); ?>
	<?php include_once("partials/nav.php"); ?>
	
	<script>
		localStorage.removeItem("products");
		if(getUrlVars()["m"] == "ord-sent") {
			$(".flash-messages").text("L'ordine e' stato preso in carico").slideToggle();
		}
	</script>

	<div class="container m-body">
		<!-- row 1 -->
		<div class="row r1">
			<div class="col-md-5 r1-b1 no-pad  rimmed-right" style="overflow:hidden;">
				<div class="b1-slider">
					<div class="slider">
						<ul class="bxslider">
							<li><a data-slide-index="1"><img src="resources/images/slide1.jpg" /></a></li>
				  			<li><a data-slide-index="2"><img src="resources/images/slide2.jpg" /></a></li>
				  			<li><a data-slide-index="3"><img src="resources/images/slide3.jpg" /></a></li>
				  			<li><a data-slide-index="4"><img src="resources/images/slide4.jpg" /></a></li>
						</ul>
						<div class="slider-btn" id="slider-next"></div>
						<div class="slider-btn" id="slider-prev"></div>
					</div>
					<div class="footer">
						<div class="f-left col-sm-7 pagination-cont">
							<div class="pagination-dots" id="bx-pager">
							    <ul>
							        <li> <a data-slide-index="0" href=""></a></li>
							        <li> <a data-slide-index="1" href=""></a></li>
							        <li> <a data-slide-index="2" href=""></a></li>
							        <li> <a data-slide-index="3" href=""></a></li>
							    </ul>
							    <div class="clear"></div>
							</div>
							<div class="clear"></div>
							<div>Lorem ipsum dolor sit amet</div>
						</div>
						<div class="f-left col-sm-5">
							<a href="dettaglio.php" class="button-scopri">scopri</a>
						</div>
					</div>
					<script>
					$(document).ready(function(){
					  	$('.bxslider').bxSlider({
						  	slideWidth: 630,
						  	slideHeight: 390,
				  		  	pagerCustom: "#bx-pager",
				  		  	nextSelector: '#slider-next',
					 	 	prevSelector: '#slider-prev',
						  	nextText: '→',
						  	prevText: '←',
						  	auto: true,
  							autoControls: true,
  							onSliderLoad: function() {
  								$(".slider").css("visibility", "visible");
  							}
					  	});
					});
					</script>
				</div>
			</div>

			<div class="col-md-3 r1-b2  rimmed-right">
				<div class="row no-pad">
					<div class="col-md-3 vertical-cont col-left"></div>
					<a class="" href="dettaglio.php"><div class="col-md-9 vertical-cont col-right"></div></a>
				</div>
			</div>

			<div class="col-md-4 r1-b3 no-pad  rimmed-left">
				<div class="top-box">
					<a href="#"><img src="./resources/images/tessutocasa.jpg" style="width:100%; height:auto;" /></a>
				</div>
				<div class="bottom-box">
					<div class="abs title">
						<a href="#">
							<img src="./resources/images/tuttocucito.png" style="" />
						</a>
					</div>
					<a href="#">
						<img src="./resources/images/ditali.jpg" style="width:100%; height:205px;" />
					</a>
				</div>
			</div>

		</div>


		<!-- row 2 -->
		<div class="row r2">
			<div class="col-md-6 rimmed-right">
				<div class="row no-pad">
					<a class="" href="dettaglio.php"><div class="col-md-7 full-h r2-b1"></div></a>
					<div class="col-md-5 full-h r2-b2">
						<div class="description-box">
							<h1>TESSUTO ZEBRA</h1>
							<p class="text">Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet</p>
							<span class="strong">costo al metro lineare</span>
							<div class="info">
								<div class="fillet">&euro;22.00</div>
							</div>
							<a href="dettaglio.php" class="strong">SCOPRI</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 rimmed-left">
				<div class="row">
					<a class="" href="dettaglio.php"><div class="col-md-7 r2-b3"></div></a>
					<div class="col-md-5 r2-b4">
						<div class="description-box">
							<h1>TESSUTO CHINTZ</h1>
							<p class="text">Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet</p>
							<span class="strong">costo al metro lineare</span>
							<div class="info">
								<div class="fillet">&euro;20.00</div>
							</div>
							<a href="dettaglio.php" class="strong">SCOPRI</a>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- row 3 -->
		<div class="row r3 no-pad">
			<div class="col-md-6  rimmed-right">
				<div class="row">
					<div class="col-md-5 r3-b1">
						<div class="description-box">
							<h1>TESSUTO BISSO</h1>
							<p class="text">Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet</p>
							<span class="strong">costo al metro lineare</span>
							<div class="info">
								<div class="fillet">&euro;11.00</div>
							</div>
							<a href="dettaglio.php" class="strong">SCOPRI</a>
						</div>
					</div>
					<a class="" href="dettaglio.php"><div class="col-md-7 r3-b2"></div></a>
				</div>
			</div>
			<div class="col-md-6 rimmed-left">
				<div class="row">
					<div class="col-md-5 r3-b3">
						<div class="description-box">
							<h1>TESSUTO ACETATO</h1>
							<p class="text">Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet</p>
							<span class="strong">costo al metro lineare</span>
							<div class="info">
								<div class="fillet">&euro;12.00</div>
							</div>
							<a href="dettaglio.php" class="strong">SCOPRI</a>
						</div>
					</div>
					<a class="" href="dettaglio.php"><div class="col-md-7 r3-b4"></div></a>
				</div>
			</div>
		</div>

	</div>	

<?php include_once("partials/footer.php");?>