<!DOCTYPE html>
<html>
	<head>
		<title>tessil style</title>
		<meta charset="utf-8" />
		<meta http-equiv="content-script-type" content="text/javascript" />
		<meta http-equiv="content-style-type"  content="text/css" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="gold" />
		<meta name="copyright" content="" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/jquery.bxslider.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<script type="text/javascript" src="./assets/js/jquery-1.11.3.min.js"></script>
		<script>
		function getUrlVars() {
			var vars = {};
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
			});
			return vars;
		}
		</script>
	</head>
	