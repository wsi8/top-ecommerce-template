<div class="container container-full">
	<div class="container no-pad">
	<nav class="navbar navbar-default">
	    <div class="navbar-header">
	      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false" aria-controls="navbar">
	        	<span class="sr-only">menu</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      	</button>
	    </div>
	    <div id="menu" class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	      	<li class="active voice-1"><a href="index.php">Abbigliamento</a></li>       
	  		<li class="voice-2"><a href="index.php">Sposa</a></li>
	      	<li class="voice-3"><a href="index.php">Casa</a></li>
	      	<li class="voice-4"><a href="index.php">Spettacolo</a></li>
	      	<li class="voice-5"><a href="index.php">Cucito</a></li>      
	      	
	      	<!-- visible-x -->
	      	<li class="voice-5 visible-xs bg-pink"><a href="carrello.php">Carrello</a></li>      
	      	<li class="voice-5 visible-xs bg-pink"><a href="index.php">Lista desideri</a></li>
	      	<li class="voice-5 visible-xs bg-pink"><a href="index.php">Login</a></li>

	      	<!-- icons -->
	      	<li class="hidden-sm hidden-xs hidden-md"><div class="col-sm-12 no-pad search-cont"><input class="input-lg input-flat full-h transparent"></div></li>
	      	<li class="voice-icon voice-icon-search hidden-sm hidden-xs hidden-md"><a href="index.php"></a></li>
	      	<li class="voice-icon voice-icon-1 hidden-sm hidden-xs"><a href="index.php"></a></li>
	      	<li class="voice-icon voice-icon-2 hidden-sm hidden-xs"><a href="index.php"></a></li>
	      	<li class="voice-icon voice-icon-3 icon-cart hidden-xs">
	      		<span class="cart-total"></span>
	      		<script>if($(".cart-total").text() == "") $(".cart-total").hide();</script>
	      		<a href="carrello.php"></a>
      		</li>      
	      </ul>
	    </div><!--/.nav-collapse -->
		</nav>
	</div>
</div>